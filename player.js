pc.script.create('player', function (context) {
    //  Constants used for the dynamic controller.
    var SPEED = 3;
    
    var origin = new pc.Vec3();
    var rayEnd = new pc.Vec3();
    var groundCheckRay;
    
    // Creates a new Player instance
    var Player = function (entity) 
    {
        this.entity = entity;
        entity.player = this;
        
        // The destination of move.
        this.destination = null;
        this._moving = false;
        
        // Dynamic controller
        this.center_height = 0.8;
        this.speed = SPEED;
        this.onGround = false;
        if (entity.collision != null)
        {
            this.center_height = entity.collision.height / 2;
        }
        groundCheckRay = new pc.Vec3(0, -(this.center_height + 0.01), 0);
    };

    Player.prototype = 
    {
        // Called once after all resources are loaded and before the first update
        initialize: function () 
        {
        },

        // Called every frame, dt is time in seconds since last update
        update: function (dt) 
        {
            var
                pos = this.entity.getPosition(), 
                dir = new pc.Vec3();

            this._checkGround();
            
            if (this.destination)
            {
                dir.sub2(this.destination, pos);
                if (dir.length() < 0.001)
                {
                    this.destination = null;
                    this._moving = false;
                }
                else
                {
                    dir.normalize();
                    this._move(dir);
                }
                
            }
        },
        
        moveTo: function (position)
        {
            this.destination = position;
            this._moving = true;
        },
        
        /**
         * Check to see if the character is standing on something
         */
        _checkGround: function () 
        {
            var self = this;
            var pos = this.entity.getPosition();
            rayEnd.add2(pos, groundCheckRay);            
            self.onGround = false;

            // Fire a ray straight down to just below the bottom of the rigid body, 
            // if it hits something then the character is standing on something.
            context.systems.rigidbody.raycastFirst(pos, rayEnd, function (result) 
            {
                self.onGround = true;
            });
        },
        
        _move: function (direction)
        {
            if (this.onGround) 
            {
                this.entity.rigidbody.activate();
                direction.scale(this.speed);                
                this.entity.rigidbody.linearVelocity = direction;
            }
        }
    };

    return Player;
});