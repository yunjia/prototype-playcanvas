pc.script.create('game', function (context) {
    // Creates a new Game instance
    var Game = function (entity) 
    {
        this.entity = entity;
        
        // Disabling the context menu stops the browser displaying a menu when 
        // you right-click the page
        context.mouse.disableContextMenu();
        
        // Listening on various mouse events
        context.mouse.on(pc.input.EVENT_MOUSEDOWN, this.onMouseDown, this);
    };

    Game.prototype = 
    {
        /**
         * Initialize all the sub-components of this game.
         * Called once after all resources are loaded and before the first update.
         */
        initialize: function () 
        {
            this.camera = this.entity.findByName('Camera').camera;
            this.player = this.entity.findByName('Player').player;
        },

        // Called every frame, dt is time in seconds since last update
        update: function (dt) 
        {
        },
        
        /**
         * Handles mouse clicking event. 
         * 1. Move the player to a specific point if clicks
         *    the floor.
         */
        onMouseDown: function (event)
        {
            var from = this.camera.entity.getPosition();
            var to = this.camera.screenToWorld(event.x, event.y, this.camera.farClip);
            
            context.systems.rigidbody.raycastFirst(from, to, function (result) 
            {
                if (result.entity.name == 'Floor')
                {
                    this.player.moveTo(result.point);
                }
            }.bind(this));
        }
    };

    return Game;
});